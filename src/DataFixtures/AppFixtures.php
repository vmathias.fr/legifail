<?php

namespace App\DataFixtures;

use App\Entity\Personne;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $personne = new Personne();
        $personne->setNom('Admin')
        ->setPassword(
            $this->passwordEncoder->encodePassword(
                $personne,
                'adminadmin'
            )
        )
        ->setRoles(array('ROLE-USER','ROLE_ADMIN'));
        $manager->persist($personne);

        $manager->flush();
    }
}
