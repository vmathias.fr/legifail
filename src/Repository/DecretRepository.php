<?php

namespace App\Repository;

use App\Entity\Decret;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Decret|null find($id, $lockMode = null, $lockVersion = null)
 * @method Decret|null findOneBy(array $criteria, array $orderBy = null)
 * @method Decret[]    findAll()
 * @method Decret[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DecretRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Decret::class);
    }

    // /**
    //  * @return Decret[] Returns an array of Decret objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Decret
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
