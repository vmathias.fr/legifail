<?php


namespace App\Controller;


use App\Entity\Decret;
use App\Entity\Instance;
use App\Entity\Personne;
use Dompdf\Dompdf;
use Dompdf\Options;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\String\Slugger\SluggerInterface;

class PageController extends AbstractController
{

    public function login(AuthenticationUtils $authentificationUtils) : Response
    {
        $error = $authentificationUtils->getLastAuthenticationError();
        $lastusername = $authentificationUtils->getLastUsername();
        return $this->render('login.html.twig', [
            'last_username' => $lastusername,
            'error' => $error
        ]);
    }

    public function logout() : void
    {
    }

    // Page d'accueil de l'application.
    // Return : Base.html.twig > accueil
    public function index() : Response {
        $repository = $this->getDoctrine()->getRepository(Decret::class);
        $decrets = $repository->findBy(
            ['actif' => 'A']
        );

        return $this->render('base.html.twig', array ('decrets' => $decrets));
    }

    // Affichage d'un décret en fonction de son id
    // Return : Decret $id
    public function decretAfficher(int $id) : Response
    {
        $repository = $this->getDoctrine()->getRepository(Decret::class);
        $decret = $repository->find($id);

        return $this->render('decret.html.twig', array('decret' => $decret));
    }

    //Affichage d'un décret au format PDF
    //Return : Vue decretpdf.html.twig avec le Decret $id
    public function decretPdf ($id) {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);
        $pdfOptions->isHtml5ParserEnabled();
        $dompdf = new Dompdf($pdfOptions);

        $repository = $this->getDoctrine()->getRepository(Decret::class);
        $decret = $repository->find($id);


        $html = $this->renderView('decretpdf.html.twig', [
            'decret' => $decret
        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $nomPdf = 'decret'.$decret->getTitre().'.pdf';
        $dompdf->stream($nomPdf, [
            "Attachment" => false,
            "isHtml5ParserEnabled" => true,
            "isRemoteEnabled" => true
        ]);
    }

    // Ajouter un nouveau décret
    //Return : Un décret sauvegardé à l'aide d'un formulaire
    public function decretAjouter(Request $request) : Response
    {
        $decret = new Decret();
        $decret->setDateInsert(new \DateTime('now'));


        $form = $this->createFormBuilder($decret)
            ->add('titre', TextType::class)
            ->add('contenu', CKEditorType::class)
            ->add('actif', ChoiceType::class, [
                'choices' => [
                    'En préparation' => 'P',
                    'Actif' => 'A'
                ]
            ])
            ->add('id_instance', EntityType::class, [
                'class' => Instance::class,
                'choice_label' => 'nom'
            ])
            ->add('save', SubmitType::class, ['label' => 'Ajouter Décret'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $decret = $form->getData();
            $user = $this->getUser();
            $decret->setAuteur($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($decret);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('decretAjouter.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function DecretModifier (Request $request, $id) : Response {
        $em = $this->getDoctrine()->getManager();
        $decret = $em->getRepository(Decret::class)->find($id);

        $form = $this->createFormBuilder($decret)
            ->add('titre', TextType::class)
            ->add('contenu', CKEditorType::class)
            ->add('actif', ChoiceType::class, [
                'choices' => [
                    'Actif' => 'A',
                    'Supprimé' => 'S'
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'Modifier Décret'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $decret = $form->getData();
            $user = $this->getUser();
            $decret->setDateUpdate(new \DateTime());
            $decret->setEditeur($user);
            if ($form->get('actif')->getData() == 'S') {
                $decret->setDateClose(new \DateTime());
            }
            $em->persist($decret);
            $em->flush();

            return $this->redirectToRoute('index');
        }
        return $this->render('decretModifier.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function instanceAjouter(Request $request, SluggerInterface $slugger) : Response
    {
        $instance = new Instance();

        $form = $this->createFormBuilder($instance)
            ->add('nom', TextType::class)
            ->add('logo', FileType::class, [
                'label' => 'Logo',
                'mapped' => false,
                'required'=> false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Le fichier doit être au format png, jpeg ou jpg.'
                    ])
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'Ajouter Décret'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $instance = $form->getData();
            $logo = $form->get('logo')->getData();
            if ($logo) {
                $originalFileName = pathinfo($logo->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFileName);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$logo->guessExtension();
                try {
                    $logo->move(
                        $this->getParameter('logoDirectory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    echo 'Une erreur s\'est produite lors de l\'lenvoi du fichier.';
                }
                $instance->setLogo($newFilename);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($instance);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('instanceAjouter.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function personneAjouter (Request $request, UserPasswordEncoderInterface $passwordEncoder) : Response {
        $em = $this->getDoctrine()->getManager();
        $personne = new Personne();

        $form = $this->createFormBuilder($personne)
            ->add('nom', TextType::class)
            ->add('password', PasswordType::class)
            ->add('instance', EntityType::class, [
                'class' => Instance::class,
                'choice_label' => 'nom'
            ])
            ->add('date_elect', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('save', SubmitType::class)
        ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $personne = $form->getData();
            $personne->setRoles(array('ROLE-USER'));
            $passEncode = $passwordEncoder->encodePassword($personne, $personne->getPassword());
            $personne->setPassword($passEncode);
            $em->persist($personne);
            $em->flush();

            return $this->redirectToRoute('index');
        }
        return $this->render('personneAjouter.html.twig', [
            'form' => $form->createView()
        ]);
    }
}