<?php

namespace App\Entity;

use App\Repository\DecretRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DecretRepository::class)
 */
class Decret
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_insert;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_update;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_apply;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_close;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $actif;

    /**
     * @ORM\ManyToOne(targetEntity=Instance::class, inversedBy="decrets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_instance;

    /**
     * @ORM\ManyToOne(targetEntity=Personne::class, inversedBy="decrets")
     */
    private $auteur;

    /**
     * @ORM\ManyToOne(targetEntity=Personne::class, inversedBy="decrets_reformes")
     */
    private $editeur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getDateInsert(): ?\DateTimeInterface
    {
        return $this->date_insert;
    }

    public function setDateInsert(\DateTimeInterface $date_insert): self
    {
        $this->date_insert = $date_insert;

        return $this;
    }

    public function getDateUpdate(): ?\DateTimeInterface
    {
        return $this->date_update;
    }

    public function setDateUpdate(?\DateTimeInterface $date_update): self
    {
        $this->date_update = $date_update;

        return $this;
    }

    public function getDateApply(): ?\DateTimeInterface
    {
        return $this->date_apply;
    }

    public function setDateApply(?\DateTimeInterface $date_apply): self
    {
        $this->date_apply = $date_apply;

        return $this;
    }

    public function getDateClose(): ?\DateTimeInterface
    {
        return $this->date_close;
    }

    public function setDateClose(?\DateTimeInterface $date_close): self
    {
        $this->date_close = $date_close;

        return $this;
    }

    public function getActif(): ?string
    {
        return $this->actif;
    }

    public function setActif(string $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getIdInstance(): ?Instance
    {
        return $this->id_instance;
    }

    public function setIdInstance(?Instance $id_instance): self
    {
        $this->id_instance = $id_instance;

        return $this;
    }

    public function getAuteur(): ?Personne
    {
        return $this->auteur;
    }

    public function setAuteur(?Personne $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getEditeur(): ?Personne
    {
        return $this->editeur;
    }

    public function setEditeur(?Personne $editeur): self
    {
        $this->editeur = $editeur;

        return $this;
    }
}
