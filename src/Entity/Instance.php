<?php

namespace App\Entity;

use App\Repository\InstanceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InstanceRepository::class)
 */
class Instance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $logo;

    /**
     * @ORM\OneToMany(targetEntity=Decret::class, mappedBy="id_instance", orphanRemoval=true)
     */
    private $decrets;

    /**
     * @ORM\OneToMany(targetEntity=Personne::class, mappedBy="instance")
     */
    private $personnes;

    public function __construct()
    {
        $this->decrets = new ArrayCollection();
        $this->personnes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return Collection|Decret[]
     */
    public function getDecrets(): Collection
    {
        return $this->decrets;
    }

    public function addDecret(Decret $decret): self
    {
        if (!$this->decrets->contains($decret)) {
            $this->decrets[] = $decret;
            $decret->setIdInstance($this);
        }

        return $this;
    }

    public function removeDecret(Decret $decret): self
    {
        if ($this->decrets->removeElement($decret)) {
            // set the owning side to null (unless already changed)
            if ($decret->getIdInstance() === $this) {
                $decret->setIdInstance(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getPersonnes(): Collection
    {
        return $this->personnes;
    }

    public function addPersonne(Personne $personne): self
    {
        if (!$this->personnes->contains($personne)) {
            $this->personnes[] = $personne;
            $personne->setInstance($this);
        }

        return $this;
    }

    public function removePersonne(Personne $personne): self
    {
        if ($this->personnes->removeElement($personne)) {
            // set the owning side to null (unless already changed)
            if ($personne->getInstance() === $this) {
                $personne->setInstance(null);
            }
        }

        return $this;
    }
}
