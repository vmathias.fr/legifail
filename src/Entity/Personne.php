<?php

namespace App\Entity;

use App\Repository\PersonneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=PersonneRepository::class)
 */
class Personne implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_elect;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_fin;

    /**
     * @ORM\ManyToOne(targetEntity=Instance::class, inversedBy="personnes")
     */
    private $instance;

    /**
     * @ORM\OneToMany(targetEntity=Decret::class, mappedBy="auteur")
     */
    private $decrets;

    /**
     * @ORM\OneToMany(targetEntity=Decret::class, mappedBy="editeur")
     */
    private $decrets_reformes;

    public function __construct()
    {
        $this->decrets = new ArrayCollection();
        $this->decrets_reformes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->nom;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getDateElect(): ?\DateTimeInterface
    {
        return $this->date_elect;
    }

    public function setDateElect(?\DateTimeInterface $date_elect): self
    {
        $this->date_elect = $date_elect;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(?\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    public function getInstance(): ?Instance
    {
        return $this->instance;
    }

    public function setInstance(?Instance $instance): self
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * @return Collection|Decret[]
     */
    public function getDecrets(): Collection
    {
        return $this->decrets;
    }

    public function addDecret(Decret $decret): self
    {
        if (!$this->decrets->contains($decret)) {
            $this->decrets[] = $decret;
            $decret->setAuteur($this);
        }

        return $this;
    }

    public function removeDecret(Decret $decret): self
    {
        if ($this->decrets->removeElement($decret)) {
            // set the owning side to null (unless already changed)
            if ($decret->getAuteur() === $this) {
                $decret->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Decret[]
     */
    public function getDecretsReformes(): Collection
    {
        return $this->decrets_reformes;
    }

    public function addDecretsReforme(Decret $decretsReforme): self
    {
        if (!$this->decrets_reformes->contains($decretsReforme)) {
            $this->decrets_reformes[] = $decretsReforme;
            $decretsReforme->setEditeur($this);
        }

        return $this;
    }

    public function removeDecretsReforme(Decret $decretsReforme): self
    {
        if ($this->decrets_reformes->removeElement($decretsReforme)) {
            // set the owning side to null (unless already changed)
            if ($decretsReforme->getEditeur() === $this) {
                $decretsReforme->setEditeur(null);
            }
        }

        return $this;
    }
}
