<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210511222611 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE decret (id INT AUTO_INCREMENT NOT NULL, id_instance_id INT NOT NULL, titre VARCHAR(255) NOT NULL, contenu LONGTEXT NOT NULL, date_insert DATETIME NOT NULL, date_update DATETIME DEFAULT NULL, date_apply DATETIME DEFAULT NULL, date_close DATETIME DEFAULT NULL, actif VARCHAR(1) NOT NULL, INDEX IDX_4271DAC62243BFEA (id_instance_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instance (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, logo VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personne (id INT AUTO_INCREMENT NOT NULL, instance_id INT DEFAULT NULL, nom VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, date_elect DATETIME DEFAULT NULL, date_fin DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_FCEC9EF6C6E55B5 (nom), INDEX IDX_FCEC9EF3A51721D (instance_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE decret ADD CONSTRAINT FK_4271DAC62243BFEA FOREIGN KEY (id_instance_id) REFERENCES instance (id)');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF3A51721D FOREIGN KEY (instance_id) REFERENCES instance (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE decret DROP FOREIGN KEY FK_4271DAC62243BFEA');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF3A51721D');
        $this->addSql('DROP TABLE decret');
        $this->addSql('DROP TABLE instance');
        $this->addSql('DROP TABLE personne');
    }
}
