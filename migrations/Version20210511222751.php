<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210511222751 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE decret ADD auteur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE decret ADD CONSTRAINT FK_4271DAC660BB6FE6 FOREIGN KEY (auteur_id) REFERENCES personne (id)');
        $this->addSql('CREATE INDEX IDX_4271DAC660BB6FE6 ON decret (auteur_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE decret DROP FOREIGN KEY FK_4271DAC660BB6FE6');
        $this->addSql('DROP INDEX IDX_4271DAC660BB6FE6 ON decret');
        $this->addSql('ALTER TABLE decret DROP auteur_id');
    }
}
